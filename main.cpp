#include <string>
#include "opencv2/opencv.hpp"
#include <opencv2/text/ocr.hpp>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include <iostream>
#include <unistd.h>


#define MIN_AR 1        //Minimum aspect ratio
#define MAX_AR 6        //Maximum aspect ratio
#define KEEP 5          //Limit the number of license plates
#define RECT_DIFF 2000  //Set the difference between contour and rectangle

//Random generator for cv::Scalar
cv::RNG rng(12345);

//COMPARE CONTOUR AREAS
bool compareContourAreas (std::vector<cv::Point>& contour1, std::vector<cv::Point>& contour2) {
    const double i = fabs(contourArea(cv::Mat(contour1)));
    const double j = fabs(contourArea(cv::Mat(contour2)));
    return (i < j);
}

//GRAY SCALE
void grayscale(cv::Mat& frame) {
  cv::cvtColor(frame, frame, cv::COLOR_BGR2GRAY);
}

//VIEW FRAME
void viewer(const cv::Mat& frame, std::string title) {
  cv::imshow(title, frame);
}


//LOCATE CANDIDATE COUNTORS
std::vector<std::vector<cv::Point>> locateCandidateCountors(cv::Mat& frame) {

  //REDUCE THE IMAGE DIMENSION TO PROCESS
  cv::Mat processedFrame = frame;
  cv::resize(frame, processedFrame, cv::Size(512, 512));

  //GRAY SCALE
  if (frame.channels() == 3) {
    grayscale(processedFrame);
  }
  //viewer(processedFrame,"Gray scale frame");

  //Perform blackhat morphological operation, reveal dark regions on light backgrounds
  cv::Mat blackHatFrame;
  cv::Mat rectangleKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(13, 5)); // Shapes are set 13 pixels wide by 5 pixels tall
  cv::morphologyEx(processedFrame, blackHatFrame, cv::MORPH_BLACKHAT, rectangleKernel);
  //viewer(blackHatFrame,"Black hat frame");

  // Find license plate based on whiteness property
  cv::Mat lightFrame;
  cv::Mat squareKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
  cv::morphologyEx(processedFrame, lightFrame, cv::MORPH_CLOSE, squareKernel);
  cv::threshold(lightFrame, lightFrame, 0, 255, cv::THRESH_OTSU);
  //viewer(lightFrame,"Light frame");

  //Compute Sobel gradient representation from blackhat using 32 float,
  //and then convert it back to normal [0, 255]
  cv::Mat gradX;
  double minVal, maxVal;
  int dx = 1, dy = 0, ddepth = CV_32F, ksize = -1;
  cv::Sobel(blackHatFrame, gradX, ddepth, dx, dy, ksize); // Looks coarse if imshow, because the range is high?
  gradX = cv::abs(gradX);
  cv::minMaxLoc(gradX, &minVal, &maxVal);
  gradX = 255 * ((gradX - minVal) / (maxVal - minVal));
  gradX.convertTo(gradX, CV_8U);
  
  //Blur the gradient result, and apply closing operation
  cv::GaussianBlur(gradX, gradX, cv::Size(5,5), 0);
  cv::morphologyEx(gradX, gradX, cv::MORPH_CLOSE, rectangleKernel);
  cv::threshold(gradX, gradX, 0, 255, cv::THRESH_OTSU);
  
  //Erode and dilate
  cv::erode(gradX, gradX, 2);
  cv::dilate(gradX, gradX, 2);

  //Bitwise AND between threshold result and light regions
  cv::bitwise_and(gradX, gradX, lightFrame);
  cv::dilate(gradX, gradX, 2);
  cv::erode(gradX, gradX, 1);
  
  //Find contours in the thresholded image and sort by size
  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(gradX, contours, cv::noArray(), cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
  //cv::drawContours(gradX, contours, -1, (0,255,367), 3);
  std::sort(contours.begin(), contours.end(), compareContourAreas);
  std::vector<std::vector<cv::Point>> top_contours;
  top_contours.assign(contours.end() - KEEP, contours.end()); // Descending order
  //cv::drawContours(gradX, top_contours, -1, (0,255,367), 3);
  return top_contours;
}


//CROP IMAGE TO GET PLATE
cv::Mat cropImageToGetLicencePlate(cv::Mat frame, std::vector<std::vector<cv::Point>>& candidates){


  const int width = frame.cols;
  const int height = frame.rows;
  const float ratio_width = width / (float) 512;    // WARNING! Aspect ratio may affect the performance (TO DO LIST)
  const float ratio_height = height / (float) 512;  // WARNING! Aspect ratio may affect the performance

  //Convert to rectangle and decide which one violate the aspect ratio and dimension relative to image input dimension.
  /*
  std::vector<cv::Rect> non_overlapping_rect;
  for (std::vector<cv::Point> currentCandidate : candidates) {
    cv::Rect rectangle_bounding = cv::boundingRect(currentCandidate);
    float aspect_ratio = rectangle_bounding.width / (float) rectangle_bounding.height;
    if (aspect_ratio >= MIN_AR && aspect_ratio <= MAX_AR && 
        rectangle_bounding.width < 0.5 * (float) frame.cols && rectangle_bounding.height < 0.5 * (float) frame.rows)  {
      float difference = rectangle_bounding.area() - contourArea(currentCandidate);
      if (difference < RECT_DIFF) {
        non_overlapping_rect.push_back(rectangle_bounding);
      } 
    }
  }
  */

  //Convert to rectangle and also filter out the non-rectangle-shape.
  std::vector<cv::Rect> rectangles;
  for (std::vector<cv::Point> currentCandidate : candidates) {
    //Calculates the up-right bounding rectangle
    cv::Rect temp = cv::boundingRect(currentCandidate);
    //Calculate difference area between contour area and bounding rectangle
    float difference = temp.area() - cv::contourArea(currentCandidate);
    if (difference < RECT_DIFF) {
      rectangles.push_back(temp);
    }
  }

  //Remove rectangle with error shape.
  rectangles.erase(std::remove_if(rectangles.begin(), rectangles.end(), [](cv::Rect temp) {
    const float aspect_ratio = temp.width / (float) temp.height;
    return aspect_ratio < MIN_AR || aspect_ratio > MAX_AR;})
    , rectangles.end());

  //If the rectangles is empty, I send default image
  if(rectangles.size()==0){
    cv::Mat defaultImg;
    defaultImg = cv::Mat(64, 64, CV_8UC3);
    return(defaultImg);
  }
  /*
  //Find overlapping rectangle and draw it (also return to the original dimension).
  for (int i = 0; i < rectangles.size(); i++) {
    bool intersects = false;
    for (int j = i + 1; j < rectangles.size(); j++) {
      if (i == j) {
        break;
      }
      intersects = ((rectangles[i] & rectangles[j]).area() > 0);
      if (intersects) {
        break;
      }
    }
    if (!intersects) {
      cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
      cv::rectangle(frame, cv::Point(rectangles[i].x * ratio_width, rectangles[i].y * ratio_height), cv::Point((rectangles[i].x + rectangles[i].width) * ratio_width, (rectangles[i].y + rectangles[i].height) * ratio_height), color, 3, cv::LINE_8, 0);
    }
  }
  */

  /*
  rectangles.erase(std::max_element(rectangles.begin(), rectangles.end(), [](cv::Rect left, cv::Rect right) {
    return left.area() < right.area();
  }));
  */
  cv::Mat licencePlate;

  for(cv::Rect rectangle : rectangles){
    cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
    //cv::rectangle(frame, cv::Point(rectangle.x * ratio_width, rectangle.y * ratio_height), cv::Point((rectangle.x + rectangle.width) * ratio_width, (rectangle.y + rectangle.height) * ratio_height), color, 3, cv::LINE_8, 0);
    frame(cv::Rect(rectangle.x * ratio_width,rectangle.y * ratio_height,rectangle.width * ratio_width,rectangle.height * ratio_height)).copyTo(licencePlate);
  }
    //PLATE: GRAY SCALE
    //grayscale(licencePlate);

    //PLATE: INVERT
    cv::Mat blackhatFrame;
    cv::Mat rectangleKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(13, 5)); // Shapes are set 13 pixels wide by 5 pixels tall
    cv::morphologyEx(licencePlate, blackhatFrame, cv::MORPH_BLACKHAT, rectangleKernel);
    return licencePlate;

}


//LICENCE PLATE RECOGNITION
void licentePlateRecognition(cv::Mat& licencePlate){

    //Create Tesseract object
    tesseract::TessBaseAPI *ocr = new tesseract::TessBaseAPI();

    ocr->Init(NULL, "eng", tesseract::OEM_DEFAULT);//OEM_LSTM_ONLY);

    //Set Page segmentation mode to PSM_AUTO (3)
    //Other important psm modes will be discussed in a future post.
    ocr->SetPageSegMode(tesseract::PSM_SINGLE_BLOCK);//PSM_SINGLE_CHAR); //PSM_AUTO);

    ocr->SetVariable("user_defined_dpi", "300");  

    //Set image data
    //ocr->SetImage(blackhatFrame.data, blackhatFrame.cols, blackhatFrame.rows, 1, blackhatFrame.step);
    ocr->SetImage((uchar*)licencePlate.data, licencePlate.size().width, licencePlate.size().height, licencePlate.channels(), licencePlate.step1());
    //Run Tesseract OCR on image
    std::string outText = std::string(ocr->GetUTF8Text());
    int confidence = ocr->MeanTextConf();
    
    if(outText.size()==0){
      ocr->End();
      return;
    }else{
      std::cout << "LICENCE PLATE: " << outText.replace(outText.find("\n",0),1,"") << "\n" << "CONFIDENCE: " << confidence << "\n" << std::endl;
      ocr->End();
      return;
    }
}

int capture( cv::VideoCapture& cap){

    cv::Mat frame;
    cap >> frame;
    if (frame.empty()) return 0;

    // Keyboard listener
    char c = (char) cv::waitKey(1);
    if (c == 200) return 0;

    //GET CANDIDATE CONTOURS
    std::vector<std::vector<cv::Point>> candidateCountors = locateCandidateCountors(frame);

    viewer(frame, "Frame");

    //GET LICENCE PLATE IMAGE
    cv::Mat licencePlate = cropImageToGetLicencePlate(frame,candidateCountors);

    //PRINT LICENCE PLATE RECOGNITION
    licentePlateRecognition(licencePlate);
    viewer(licencePlate, "Licence plate");

    frame.release();
    licencePlate.release();
    candidateCountors.clear();
    //sleep(1);
    //cv::waitKey(30);
    return 1;
}




int main(int argc, char** argv){


//IMAGE*********************************************************************************
  //LOAD FRAME
  std::string filename = "data/001.jpg";
  cv::Mat frame =  cv::imread(filename, cv::IMREAD_COLOR);
  if(!frame.data){
    std::cout <<  "Image not found or unable to open" << std::endl ;
    return -1;
  }

 //GET CANDIDATE CONTOURS
  std::vector<std::vector<cv::Point>> candidateCountors = locateCandidateCountors(frame);

  //GET LICENCE PLATE IMAGE
  cv::Mat licencePlate = cropImageToGetLicencePlate(frame,candidateCountors);

  //PRINT LICENCE PLATE RECOGNITION
  licentePlateRecognition(licencePlate);
  viewer(licencePlate, "Licence plate");

  viewer(frame,"Image");
  cv::waitKey(0);
//**************************************************************************************  

/*
//VIDEO*********************************************************************************
  //INITIALIZE VIDEOCAPTURE
  cv::VideoCapture cap;
  //Open the default camera using default API
  //cap.open(0);
  //OR advance usage: select any API backend
  int deviceID = 0;             // 0 = open default camera
  int apiID = cv::CAP_ANY;      // 0 = autodetect default API
  // open selected camera using selected API
  cap.open(deviceID);//, apiID);
  // check if we succeeded
  if (!cap.isOpened()) {
    std::cerr << "ERROR! Unable to open camera\n";
    return -1;
  }

  while (true) {
    if(capture(cap)==0 ){
      break;
    }
  }

  cap.release();
  cv::destroyAllWindows();
//**************************************************************************************  
*/
  return 0;
}